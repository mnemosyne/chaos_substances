"""
Names of common 
    - liquids and fluids
    - metals
    - solids
    - gases
"""


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##


import argparse
import doctest
import inspect
import sys
# ~ import pandas


##======================================================================================================================##
##                SUBSTANCES NAMES                                                                                      ##
##======================================================================================================================##

AIR_NM = "air"
DRY_AIR_NM = "dry air"
WATER_NM = "H2O"
WATER_VAPOR_NM = "water vapor"
LIQUID_WATER_NM = "liquid water"
WATER_ICE_NM = "water ice"
CONCRETE_NM = "concrete"
CONCRETE_BLOCK_NM = "concrete block"


LIGHT_WOOD_NM = "light wood"
HEAVY_WOOD_NM = "heavy wood"
WOOD_PANEL_NM = "wood panel"
GLASS_WOOL_NM = "glass wool"
STONE_WOOL_NM = "stone wool"
POLYSTYRENE_NM = "polystyrene"
POLYURETHANE_NM = "polyurethane"
WOOD_WOOL_NM = "wood wool"
BIOFIB_TRIO_NM = "biofib trio"
BIOFIB_CHANVRE_NM = "biofib chanvre"
BIOFIB_PANO_NM = "biofib pano"
GYPSUM_BOARD_NM = "gypsum board"
GYPSUM_NM = "gypsum"
OSB_NM = "OSB"
LIME_NM = "lime"
IRON_NM = "iron"
STAINLESS_STEEL_NM = "stainless steel"
PROCLIMAT_DBPLUS_NM = "proclimat db+"
GLASS_NM = "glass"
LOW_EMISSIVITY_GLASS_NM = "low-e glass"
REFLECTING_GLASS_NM = "reflecting glass"
HEAT_ABSORBING_GLASS_NM = "heat-absorbing glass"
SLATE_ROOF_NM = "slate roof"

H2_NM = "Hydrogen"

CH4_NM = "Methane"

C2H6_NM ="Ethane"
C3H8_NM = "Propane"
C4H10_NM = "Butane"
C5H12_NM = "Pentane"

KEROSENE_NM = "Kerosene"
GASOIL_NM = "Gasoil"
DIESEL_NM = "Diesel"
WOOD_FUEL_NM = "Wood fuel"

BLACK_COAL = "black coal" # Anthracite
BROWN_COAL = "brown coal" #Lignite

##======================================================================================================================##
##                GET ALL SUBSTANCES NAMES                                                                              ##
##======================================================================================================================##

SUBSTANCE_NAMES = sorted( [i[1] for i in inspect.getmembers(sys.modules[__name__]) if i[0].upper() == i[0] and isinstance(i[1], str)])



##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--examples', default=False, action='store_true')
    opts = parser.parse_args()


    #+++++++++++++++++++++++++++++++#
    #        Doctests               #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
        
        
    #+++++++++++++++++++++++++++++++#
    #    examples                   #
    #+++++++++++++++++++++++++++++++#
    if opts.examples:
        pass


    print(SUBSTANCE_NAMES)
    
