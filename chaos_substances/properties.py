"""
properties names
"""


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import os
import argparse
import doctest
import inspect
import sys


##======================================================================================================================##
##                STD PROPERTIES FUNCTIONS                                                                              ##
##======================================================================================================================##

def std_propertie(prop):
    """
    Standardize propertie
    """
    res = prop.lower()
    for k in ("-", " ",):
        res = res.replace(k, "_")
        
    return res


def is_std_propertie(prop):
    """
    Get properties
    """
    res = prop == std_propertie(prop)
    return res

##======================================================================================================================##
##                SUBSTANCES PROPERTIES                                                                                 ##
##======================================================================================================================##


MOLAR_MASS = "molar_mass"
THERMAL_CONDUCTIVITY = "thermal_conductivity"
SPECIFIC_MASS = "specific_mass"
SPECIFIC_HEAT_CAPACITY = "specific_heat_capacity"
WATER_VAPOUR_PERMEABILITY = "water_vapour_permeability"
# ~ SD = "sd"
SW_REFLECTIVITY = "sw_reflectivity"
SW_TRANSMISSIVITY = "sw_transmissivity"


IR_EMISSIVITY = "ir_emissivity"

LHV = "lower_heating_value"
HHV = "higher_heating_value"

##======================================================================================================================##
##             GET ALL SUBSTANCES PROPERTIES                                                                            ##
##======================================================================================================================##

PROPERTIES_NAMES = sorted([i[1] for i in inspect.getmembers(sys.modules[__name__]) if i[0].upper() == i[0] and isinstance(i[1], str)])

assert (all([is_std_propertie(i) for i in PROPERTIES_NAMES]))


_constant_dir = __file__.replace('.py', "")
assert os.path.isdir(_constant_dir), "{0} is not a dir".format(_constant_dir)


# ~ print("prporiete thermique ici: https://energieplus-lesite.be/donnees/enveloppe44/enveloppe2/conductivite-thermique-des-materiaux/")
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--examples', default=False, action='store_true')
    opts = parser.parse_args()


    #+++++++++++++++++++++++++++++++#
    #        Doctests               #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
        
        
    #+++++++++++++++++++++++++++++++#
    #    examples                   #
    #+++++++++++++++++++++++++++++++#
    if opts.examples:
        pass

    print(PROPERTIES_NAMES)
