"""
Substance module


>>> print(RHO_LIQUID_WATER)
specific mass of liquid water
rho = 1000.0 kg/m3

>>> print(CP_LIQUID_WATER)
specific heat capacity of liquid water
Cp = 4187.0 J/kg/K

"""


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

# ~ from physics.units import *
# ~ from physics.types import PhysicVar, Temperature

from measurement.measures import Temperature

from apollon_physics import PhysicConstant

from chaos_substances.names import *
from chaos_substances.properties import *
from chaos_substances.get_properties import PROPERTIES_INFOS, PROPERTIES_TABLE


##======================================================================================================================##
##               SET CONSTANTS                                                                                          ##
##======================================================================================================================##

# ~ TA_KEY = "t_air"
# ~ TD_KEY = "t_dew"
# ~ RH_KEY = "rh"
# ~ E_KEY = "e"
# ~ ESAT_KEY = "es"



#TEMPERATURE    #
#---------------#



#~ T0K_CELSIUS = PhysicCst(name="T0", value = , unit = "C", infos="Temperature in Celsius of absolute zero")

TF_WATER_C = Temperature(C=0) #Freezing temperature of Water, Celsius"
#~ TF_WATER_K = PhysicCst(name="Tf", value=- T0K_CELSIUS, unit="K", infos = "Freezing temperature of Water, Kelvin")
TB_WATER_C = Temperature(c=100)#"Boiling temperature of Water, Celsius"
#~ TB_WATER_K = PhysicCst(name="Tb", value=TB_WATER_C - T0K_CELSIUS, unit="K", infos="Boiling temp of Water, Kelvin")


#MOLECULAR MASS #
#---------------#


# ~ from functions import todo_msg
# ~ print(todo_msg("check automatic name and value", __file__))

M_DRYAIR = PROPERTIES_TABLE.loc[DRY_AIR_NM, MOLAR_MASS]
M_H2O = PROPERTIES_TABLE.loc[WATER_NM, MOLAR_MASS]

# ~ print(M_DRYAIR)
# ~ print(M_H2O)

# ~ M_DRYAIR = PhysicVar(name="Mdryair", value=28.96e-3, unit="kg/mol", infos="Molecular mass of a dry atmosphere")
# ~ M_H2O = PhysicVar(name="Mwater", value=18.02e-3, unit="kg/mol", infos="Molecular mass of water")


# ~ print(M_DRYAIR)
# ~ print(M_H2O)

# ~ raise

#VOLUMIC MASSES #
#---------------#

# ~ from functions import todo_msg
# ~ print(todo_msg("check automatic name and value", __file__))

RHO_LIQUID_WATER = PROPERTIES_TABLE.loc[LIQUID_WATER_NM, SPECIFIC_MASS]

# ~ RHO_WATER = PhysicVar(name="rho_water", value=1000, unit="kg/ m3", infos="volumic mass of water")


# ~ print(RHO_WATER)
# ~ print(RHO_LIQUID_WATER)

#SPECIFIC HEAT  #
#---------------#

# ~ from functions import todo_msg
# ~ print(todo_msg("check automatic name and value", __file__))

CP_LIQUID_WATER = PROPERTIES_TABLE.loc[LIQUID_WATER_NM, SPECIFIC_HEAT_CAPACITY]
CP_WATER_ICE = PROPERTIES_TABLE.loc[WATER_ICE_NM, SPECIFIC_HEAT_CAPACITY]
CP_DRYAIR = PROPERTIES_TABLE.loc[DRY_AIR_NM, SPECIFIC_HEAT_CAPACITY]
CP_WATER_VAPOR = PROPERTIES_TABLE.loc[WATER_VAPOR_NM, SPECIFIC_HEAT_CAPACITY]


# ~ print(CP_LIQUID_WATER)
# ~ print(CP_WATER_ICE)
# ~ print(CP_DRYAIR)
# ~ print(CP_WATER_VAPOR)



# ~ CP_WATER = PhysicVar(name="Cp-wr", value=4.187e3, unit="J/kg/K", infos='Specific heat of water |normal atm')
# ~ CP_ICE = PhysicVar(name="Cp-i", value=2.108e3, unit="J/kg/K", infos='Specific heat of ice |normal atm')
# ~ CP_DRYAIR = PhysicVar(name="Cp-dryair", value=1004.5, unit="J/kg/K", infos='Specific heat of dry air |normal atm at cst P')
# ~ CP_WVAPOR = PhysicVar(name="Cp-wvapor", value=1850, unit="J/kg/K", infos='Specific heat of dry air |normal atm at cst P')


# ~ print(CP_WATER)
# ~ print(CP_ICE)
# ~ print(CP_DRYAIR)
# ~ print(CP_WVAPOR)




#CV AT CONSTANT VOLUME = CPDRYAIR - RDRYAIR SEE https://fr.wikipedia.org/wiki/Relation_de_Mayer

#Latent heats   #
#---------------#

LV_WATER = PhysicConstant(name="Lv", value=2.50e6, unit="J/kg", infos="Latent heat of water Vaporization |normal atm")
LM_WATER = PhysicConstant(name="Lm", value=3.34e5, unit="J/kg", infos='Latent heat of water Melting |normal atm')


#WV PERMEABILITY#
#---------------#

WV_PERMEABILITY_OF_AIR = PROPERTIES_TABLE.loc[AIR_NM, WATER_VAPOUR_PERMEABILITY]

# ~ print(WV_PERMEABILITY_OF_AIR)




##======================================================================================================================##
##                                FUNCTION                                                                              ##
##======================================================================================================================##

    
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    import argparse
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--examples', default=False, action='store_true')
    opts = parser.parse_args()


    #+++++++++++++++++++++++++++++++#
    #        Doctests               #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        import doctest
        test_result = doctest.testmod()
        print(test_result)
        
        
    #+++++++++++++++++++++++++++++++#
    #    examples                   #
    #+++++++++++++++++++++++++++++++#
    if opts.examples:
        pass
        
