"""
Python module containing physic constants
"""


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import os
import argparse
import doctest
import glob
import pandas

from apollon_physics import PhysicConstant
from apollon_physics.types import _infoky, _sym_key

from chaos_substances.names import SUBSTANCE_NAMES
from chaos_substances.properties import PROPERTIES_NAMES, _constant_dir, std_propertie










##======================================================================================================================##
##                SET CONSTANTS                                                                                         ##
##======================================================================================================================##

_exp_colanmes = ("value", "unit", _sym_key, _infoky)


CSVFILES = glob.glob(os.path.join(_constant_dir, "*.csv")) 
   
_tables0 = {os.path.splitext(os.path.basename(ifile))[0].replace("_", " "): pandas.read_csv(ifile, index_col=0) for ifile in CSVFILES}
# ~ print(_tables0)
_tables1 = {k: pandas.Series(tab.index.map(lambda x: " of ".join([k, x])), index=tab.index, name=_infoky) for k, tab in _tables0.items()}

PROPERTIES_INFOS = {std_propertie(k) : pandas.concat([tab, _tables1[k]], axis=1) for k, tab in _tables0.items()}

# ~ print(PROPERTIES_INFOS)



#CHECK
for _k, _v in PROPERTIES_INFOS.items():
    assert sorted(_v.columns) == sorted(_exp_colanmes), "pb columns\n{0}".format(_v)
    for _index in _v.index:
        assert _index in SUBSTANCE_NAMES, "'{0}' not in\n{1}".format(_index, SUBSTANCE_NAMES)


    assert not _v.index.duplicated(False).sum(), "There are some duplicates in {0}\n{1}".format(_k, _v[_v.index.duplicated(False)])
    assert _k in PROPERTIES_NAMES, "{0} not in\n{1}".format(_k, PROPERTIES_NAMES)
    
    
del _k, _v, _index, _tables0, _tables1



_dic0 = {k:df.apply(dict, axis=1) for k, df in PROPERTIES_INFOS.items()}
_dic1 = {k: ts.apply(lambda x: PhysicConstant(**x), convert_dtype=False) for k, ts in _dic0.items()}


PROPERTIES_TABLE = pandas.DataFrame(_dic1)

del _dic0, _dic1

# ~ for i in list_csv_files():
    # ~ print(get_char_from_csvfile(i))
    # ~ tab = read_csvfile(i)
    # ~ print(tab)



# ~ char = get_char_from_csvfile(csvfile=csvfile)
# ~ infos = 
# ~ res = 









##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--examples', default=False, action='store_true')
    opts = parser.parse_args()


    #+++++++++++++++++++++++++++++++#
    #        Doctests               #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
        
        
    #+++++++++++++++++++++++++++++++#
    #    examples                   #
    #+++++++++++++++++++++++++++++++#
    if opts.examples:
        pass
        
        
    for k, v in PROPERTIES_INFOS.items():
        print(k)
        print(v)
        print("\n")
        
    
    print("concatenated")
    
    print(PROPERTIES_TABLE)

    print(PROPERTIES_TABLE.iloc[-1,0])
    
